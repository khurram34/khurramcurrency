import React, {Component} from 'react';
import {Text, View} from 'react-native';

import Currency from './Screens/Currency';
import Home from './Screens/Home';
import Login from './Screens/Login';
import Logout from './Screens/Logout';
import {NavigationContainer} from '@react-navigation/native';
import Option from './Screens/Option';
import { ThemeContextProvider } from './Screens/ThemeProvieder';
import Signup from './Screens/Signup';
import Splash from './Screens/Splash';
import Theme from './Screens/Theme';
import {createStackNavigator} from '@react-navigation/stack';
console.disableYellowBox= true
const Stack = createStackNavigator();
class signin extends Component {
  render() {
    return (
      <View>
        <Text>signin</Text>
      </View>
    );
  }
}
class Detail extends Component {
  render() {
    return (
      <View>
        <Text>Detail</Text>
      </View>
    );
  }
}

class App extends Component {
  render() {
    return (
         <ThemeContextProvider {...this.props}>
          <NavigationContainer initialRouteName={Splash}>
            <Stack.Navigator headerMode="none" initialRouteName={Splash}>
              <Stack.Screen name="Splash" component={Splash} options={{}}/>
              <Stack.Screen name="Login" component={Login} options={{}}/>
              <Stack.Screen name="Signup" component={Signup} options={{}}/>
              <Stack.Screen name="Home" component={Home} options={{}}/>
              <Stack.Screen name="Currency" component={Currency} options={{}}/>
              <Stack.Screen name="Theme" component={Theme} options={{}}/>
              <Stack.Screen name="Option" component={Option} options={{}}/>
              <Stack.Screen name="Logout" component={Logout} options={{}}/>
            </Stack.Navigator>
          </NavigationContainer>
         </ThemeContextProvider>
    );
  }
}

export default App;
