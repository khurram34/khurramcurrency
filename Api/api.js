import axios from 'react-native-axios';

axios.create({
  baseURL: 'https://data.fixer.io',
  headers: {
    Authorization: 'Bearer de89d6fef8c98bb59c423371fb4bad30',
  },
});
