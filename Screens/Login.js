import {
  Alert,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import {NavigationActions, StackActions} from '@react-navigation/native';
import React, {Component} from 'react';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';

import AsyncStorage from '@react-native-community/async-storage';
import Loader from './Loader';
import Services from '../services/Services';

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      loading: false,
    };
  }
  onLoginPress = () => {
    this.setState({loading: true});
    const {email, password} = this.state;
    if (email === '') {
      this.setState({loading: false});
      Alert.alert('Please enter email address');
    } else if (password === '') {
      this.setState({loading: false});
      Alert.alert('Please enter password');
    } else {
      Services.userLogin(email, password, response => {
        let user = response.response;
        AsyncStorage.setItem('USER', JSON.stringify(user));
        if (response.isSuccess) {
          // const navigateAction = StackActions.reset({
          //   index: 0,
          //   routeName: 'Home',
          //   actions: [NavigationActions.navigate({routeName: 'Home'})],
          // });
          this.props.navigation.navigate('Home');
          this.setState({loading: false});
        } else {
          console.log('error', response.error);
          this.setState({loading: false});
          alert(response.error);
        }
      });
    }
  };
  // onSuccessLogin = id => {
  //   service.getCurrentUserProfile(id, profile => {
  //     if (profile.userProfile.customer) {
  //       this.setState({loading: false});
  //       Alert.alert('Please Enter a Valid Email And Password');
  //     } else {
  //       const navigateAction = StackActions.reset({
  //         index: 0,
  //         routeName: 'Home',
  //         actions: [NavigationActions.navigate({routeName: 'Home'})],
  //       });
  //       this.setState({loading: false});
  //       this.props.navigation.dispatch(navigateAction);
  //     }
  //   });
  // };
  render() {
    return (
      <View>
        <View>
          <Loader loading={this.state.loading} />
          <Text
            style={{
              fontSize: widthPercentageToDP(15),
              alignSelf: 'center',
              marginTop: heightPercentageToDP(20),
              marginBottom: heightPercentageToDP(5),
              fontWeight: 'bold',
            }}>
            LOGIN
          </Text>
          <View style={styles.inPutContainerStyle}>
            <Text style={styles.textStyle}>Email</Text>
            <TextInput
              placeholder="Email"
              style={styles.inPutStyle}
              value={this.state.email}
              onChangeText={email => this.setState({email})}
            />
          </View>
          <View style={styles.inPutContainerStyle}>
            <Text style={styles.textStyle}>Password</Text>
            <TextInput
              placeholder="Password"
              style={styles.inPutStyle}
              value={this.state.password}
              onChangeText={password => this.setState({password})}
            />
          </View>
          <TouchableOpacity
            style={styles.buttonStyle}
            onPress={this.onLoginPress}>
            <Text style={styles.textStyle}>Login</Text>
          </TouchableOpacity>
          <Text
            style={[
              styles.textStyle,
              {marginTop: heightPercentageToDP(2), alignSelf: 'center'},
            ]}
            onPress={() => this.props.navigation.navigate('Signup')}>
            Don't have an account? Signup
          </Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  inPutContainerStyle: {
    width: widthPercentageToDP(80),
    alignSelf: 'center',
  },
  inPutStyle: {
    backgroundColor: 'white',
    borderWidth: 1,
    borderColor: '#f3f3f3',
    elevation: 1,
    paddingLeft: widthPercentageToDP(2),
    borderRadius: widthPercentageToDP(1),
    marginBottom: widthPercentageToDP(1),
  },
  textStyle: {
    marginBottom: heightPercentageToDP(1),
    fontSize: 20,
    fontWeight: 'bold',
  },
  buttonStyle: {
    backgroundColor: 'tomato',
    width: widthPercentageToDP(60),
    alignSelf: 'center',
    marginTop: heightPercentageToDP(5),
    height: heightPercentageToDP(7),
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: widthPercentageToDP(1),
  },
});
export default Login;
