import React from 'react';
import {View, Text, Picker, TextInput} from 'react-native'
import {heightPercentageToDP, widthPercentageToDP} from 'react-native-responsive-screen';

export default function CurrencyRow(props) {

    const {
        currencyOptions,
        selectedCurrency,
        onChangeCurrency,
        onChangeAmount,
        amount,
        pickerColor,
        textColor
    } = props
console.log("amount", amount)
    return (
        <View>
            <View style={{
                width: widthPercentageToDP(90),
                backgroundColor: 'white',
                flexDirection: 'row',
                justifyContent: 'center',
                alignSelf: 'center',
            }}>
                <Picker
                    selectedValue={selectedCurrency}
                    style={{
                        height: 50,
                        width: widthPercentageToDP(30),
                        justifyContent: 'center',
                        alignItems: 'center',
                        backgroundColor: pickerColor,
                    }}
                    onValueChange={onChangeCurrency}
                >
                    {currencyOptions !== undefined ? currencyOptions.map(option => {
                        return (
                            <Picker.Item key={option} label={option} value={option}/>
                        );
                    }) : 0}

                </Picker>
                <TextInput style={{color: 'black'}} defaultValue={amount} value={amount} placeholder="100"
                           onChangeText={onChangeAmount} style={{width: widthPercentageToDP(60)}}/>
            </View>
            <Text style={{alignSelf: 'center', color:textColor, fontWeight:"bold", fontSize: 18}}>{amount}</Text>
        </View>
    );
}
