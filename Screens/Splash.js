import {Image, Text, View} from 'react-native';
import React, {Component} from 'react';

import AsyncStorage from '@react-native-community/async-storage';
import Images from '../Images/image';

class Splash extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  componentDidMount() {
    AsyncStorage.getItem('USER').then(resp => {
      const response = JSON.parse(resp);
      if (response !== null) {
        this.props.navigation.navigate('Home');
      } else {
        this.props.navigation.navigate('Login');
      }
    });
    // setTimeout(() => {
    //     this.props.navigation.navigate("onBoarding")
    // }, 2000)
  }
  render() {
    return (
      <View style={styles.containerStyle}>
        <Image
          source={Images.logo}
          style={styles.ImageStyle}
          resizeMode="contain"
        />
      </View>
    );
  }
}
const styles = {
  containerStyle: {
    backgroundColor: '#506d79',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  ImageStyle: {
    height: '40%',
    width: '40%',
  },
};
export default Splash;
