import {FlatList, Text, TouchableOpacity, View} from 'react-native';
import React, {Component} from 'react';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import {theme, themes, withTheme} from './ThemeProvieder';
import Header from './Header';
function Theme({theme, themes, setTheme, navigation}) {
    function renderItem({item, navigation}) {

        return (
            <View style={{paddingBottom: hp(2), elevation: 1, borderBottomWidth: 1}}>

                <TouchableOpacity onPress={() => setTheme(item.key)} style={{
                    height: hp(10),
                    width: wp(80),
                    flexDirection: 'row',
                    alignItems: 'center',
                    paddingLeft: wp(2)


                }}>
                    <Text style={{width: wp('80')}}>{item.key}</Text>

                    <View style={{
                        alignSelf: 'flex-end',
                        backgroundColor: item.backgroundColor,
                        height: hp(6),
                        width: wp(8),
                        borderRadius:wp(100)
                    }}></View>
                </TouchableOpacity>
            </View>
        );
    }


    return (
        <View>
            <Header headerText={"Theme"}/>
            <FlatList data={themes} renderItem={renderItem}/>
        </View>

    );
}







  // onItemPress = theme => {
  //   console.log('theme=====>', theme.backgroundColor);
  //   AsyncStorage.setItem('backgroundColor', theme.backgroundColor).then(() => {
  //     this.props.navigation.navigate('Home', {theme: theme});
  //     console.log('success');
  //   });
  // };
  // renderColor = item => {
  //   console.log('item====>', item);
  //   return (
  //     <TouchableOpacity
  //       style={styles.cardStyle}
  //       onPress={() => this.onItemPress(item.item)}>
  //       <Text style={{width: '80%'}}>{item.item.key}</Text>
  //       <View
  //         style={{
  //           width: '10%',
  //           backgroundColor: item.item.backgroundColor,
  //           borderBottomWidth: 1,
  //           justifyContent: 'center',
  //           borderRadius: 20,
  //           height: 40,
  //           marginTop: '5%',
  //           marginBottom: '5%',
  //         }}></View>
  //     </TouchableOpacity>
  //   );
  // };
// }
const styles = {
  cardStyle: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 0.5,
    marginBottom: 5,
    width: '90%',
    alignSelf: 'center',
    elevation: 1,
  },
  // colorContainerStyle: {
  //   width: '10%',
  //   backgroundColor: item.item.backgroundColor,
  //   borderWidth: 1,
  //   justifyContent: 'center',
  //   borderRadius: 20,
  //   height: 40,
  //   width: 40,
  //   marginTop: '5%',
  //   marginBottom: '5%',
  // },
};

export default withTheme(Theme);
