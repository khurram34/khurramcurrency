import {
  Alert,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import {NavigationActions, StackActions} from '@react-navigation/native';
import React, {Component} from 'react';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';

import AsyncStorage from '@react-native-community/async-storage';
import Loader from './Loader';
import Services from '../services/Services';

class Signup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      name: '',
      loading: false,
    };
  }
  onSignupPress = () => {
    this.setState({loading: true});
    const {email, name, password} = this.state;
    if (email === '') {
      this.setState({loading: false});
      Alert.alert('Please enter email address');
    } else if (name === '') {
      this.setState({loading: false});
      Alert.alert('Please enter name');
    } else if (password === '') {
      this.setState({loading: false});
      Alert.alert('Please enter password');
    } else {
      Services.createNewUser(email, password, response => {
        if (response.isSuccess) {
          console.log('user==========>', response);
          this.setState({loading: false});
          let user = response.response;
          AsyncStorage.setItem('USER', JSON.stringify(user));
          this.props.navigation.navigate('Home');
        } else {
          this.setState({loading: false});
          Alert.alert(response.error);
        }
      });
    }
  };

  render() {
    return (
      <View>
        <View>
          <Text
            style={{
              fontSize: widthPercentageToDP(15),
              alignSelf: 'center',
              marginTop: heightPercentageToDP(13),
              marginBottom: heightPercentageToDP(5),
              fontWeight: 'bold',
            }}>
            SIGNUP
          </Text>
          <Loader loading={this.state.loading} />
          <View style={styles.inPutContainerStyle}>
            <Text style={styles.textStyle}>Name</Text>
            <TextInput
              placeholder="Name"
              style={styles.inPutStyle}
              value={this.state.name}
              onChangeText={name => this.setState({name})}
            />
          </View>
          <View style={styles.inPutContainerStyle}>
            <Text style={styles.textStyle}>Email</Text>
            <TextInput
              placeholder="Email"
              style={styles.inPutStyle}
              value={this.state.email}
              onChangeText={email => this.setState({email})}
            />
          </View>
          <View style={styles.inPutContainerStyle}>
            <Text style={styles.textStyle}>Password</Text>
            <TextInput
              placeholder="Password"
              style={styles.inPutStyle}
              value={this.state.password}
              onChangeText={password => this.setState({password})}
            />
          </View>
          <TouchableOpacity
            style={styles.buttonStyle}
            onPress={this.onSignupPress}>
            <Text style={styles.textStyle}>Sign up</Text>
          </TouchableOpacity>
          <Text
            style={[
              styles.textStyle,
              {marginTop: heightPercentageToDP(2), alignSelf: 'center'},
            ]}
            onPress={() => this.props.navigation.navigate('Login')}>
            Don't have an account? Login
          </Text>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  inPutContainerStyle: {
    width: widthPercentageToDP(80),
    alignSelf: 'center',
  },
  inPutStyle: {
    backgroundColor: 'white',
    borderWidth: 1,
    borderColor: '#f3f3f3',
    elevation: 1,
    paddingLeft: widthPercentageToDP(2),
    borderRadius: widthPercentageToDP(1),
    marginBottom: heightPercentageToDP(1),
  },
  textStyle: {
    marginBottom: heightPercentageToDP(1),
    fontSize: 20,
    fontWeight: 'bold',
  },
  buttonStyle: {
    backgroundColor: 'tomato',
    width: widthPercentageToDP(60),
    alignSelf: 'center',
    marginTop: heightPercentageToDP(5),
    height: heightPercentageToDP(7),
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: widthPercentageToDP(1),
  },
});
export default Signup;
