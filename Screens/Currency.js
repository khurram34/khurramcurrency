import {FlatList, Image, Text, View} from 'react-native';
import React, {Component} from 'react';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';

import Header from './Header';
import Images from '../Images/image';

class Currency extends Component {
  constructor(props) {
    super(props);
    this.state = {
      lan: [
        {
          index: 0,
          lan: 'USD',
        },
        {
          index: 1,
          lan: 'EUR',
        },
        {
          index: 2,
          lan: '$',
        },
        {
          index: 3,
          lan: 'PKR',
        },
        {
          index: 4,
          lan: 'DEF',
        },
        {
          index: 5,
          lan: 'PRF',
        },
        {
          index: 6,
          lan: 'TKS',
        },
        {
          index: 7,
          lan: 'ASD',
        },
        {
          index: 8,
          lan: 'KHG',
        },
        {
          index: 9,
          lan: 'TYU',
        },
      ],
    };
  }
  render() {
    return (
      <View>
        <Header
          headerText="Currency"
          onPress={() => this.props.navigation.goBack()}
        />
        <FlatList data={this.state.lan} renderItem={this.renderItem} />
      </View>
    );
  }
  renderItem = item => {
    return (
      <View
        style={{
          flexDirection: 'row',
          borderBottomWidth: 1,
          width: widthPercentageToDP(100),
          height: heightPercentageToDP(8),
          marginBottom: heightPercentageToDP(1),
          elevation: 1,
          borderColor: '#f3f3f3',
          justifyContent: 'center',
          alignItems: 'center',
          paddingLeft: widthPercentageToDP(5),
        }}>
        <View>
          <Text
            style={{
              fontSize: 18,
              fontWeight: 'bold',
              width: widthPercentageToDP(70),
            }}>
            {item.item.lan}
          </Text>
        </View>
        <View
          style={{
            width: widthPercentageToDP(25),
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Image
            source={Images.uncheck}
            style={{
              height: heightPercentageToDP(6),
              width: widthPercentageToDP(6),
            }}
            resizeMode="contain"
          />
        </View>
      </View>
    );
  };
}

export default Currency;
