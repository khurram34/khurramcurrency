import {Image, Text, TouchableOpacity, View} from 'react-native';
import React, {Component} from 'react';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';

import Header from './Header';
import Images from '../Images/image';

class Option extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <View>
        <Header
          headerText="Option"
          onPress={() => this.props.navigation.goBack()}
        />
        <View style={{}}>
          <TouchableOpacity
            style={styles.cardStyle}
            onPress={() => this.props.navigation.navigate('Theme')}>
            <View style={styles.TextContainerStyle}>
              <Text style={{fontSize: 18}}>Themes</Text>
            </View>
            <View style={styles.IconStyle}>
              <Image
                source={Images.arrowf}
                resizeMode="contain"
                style={{height: hp(10), width: wp(10)}}
              />
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={styles.cardStyle}>
            <View style={styles.TextContainerStyle}>
              <Text style={{fontSize: 18}}>Fixer.io</Text>
            </View>
            <View style={styles.IconStyle}>
              <Image
                source={Images.arrowf}
                resizeMode="contain"
                style={{height: hp(10), width: wp(10)}}
              />
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.cardStyle}
            onPress={() => this.props.navigation.navigate('Logout')}>
            <View style={styles.TextContainerStyle}>
              <Text style={{fontSize: 18}}>Sign Out</Text>
            </View>
            <View style={styles.IconStyle}>
              <Image
                source={Images.logout}
                resizeMode="contain"
                style={{height: hp(10), width: wp(10)}}
              />
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
const styles = {
  cardStyle: {
    backgroundColor: 'white',
    borderRadius: wp(1),
    marginBottom: hp(1),
    flexDirection: 'row',
    height: hp(10),
    elevation: 1,
    borderWidth: 1,
    borderColor: '#f3f3f3',
    width: wp(96),
    paddingLeft: wp(5),
    alignItems: 'center',
    alignSelf: 'center',
  },
  IconStyle: {
    width: wp(20),
    justifyContent: 'center',
    alignItems: 'center',
    height: hp(10),
  },
  TextContainerStyle: {
    width: wp(70),
    height: hp(10),
    justifyContent: 'center',
  },
};
export default Option;
