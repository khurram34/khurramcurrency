import {
  Image,
  Picker,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import {
  heightPercentageToDP,
  heightPercentageToDP as hp,
  widthPercentageToDP,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import {theme, themes} from './ThemeProvieder';
import CurrencyRow from './CurrencyRow';
import THEMES from './Theme.json';

// export const themes = THEMES;
// export const theme = THEMES[1];
import Header from './Header';
import Images from '../Images/image';
import axios from 'react-native-axios';
import { withTheme } from './ThemeProvieder';
import Loader from './Loader';
const BASE_URL =
  'http://data.fixer.io/api/symbols?access_key=8fe97d1c43757594092a2b64639e006b';

function Home({theme, navigation }) {

    const [currencyOptions, setCurrencyOptions] = useState([]);
    const [fromCurrency, setFromCurrency] = useState();
    const [toCurrency, setToCurrency] = useState();
    const [exchangeRate, setExchangeRate] = useState();
    const [amount, setAmount] = useState(1);
    const [amountInFromCurrency, setAmountInFromCurrency] = useState(true);
    const [loading , setLoading] = useState(false)

    let toAmount, fromAmount;
    if (amountInFromCurrency) {
        fromAmount = amount;
        toAmount = amount * exchangeRate;
    } else {
        toAmount = amount;
        fromAmount = amount / exchangeRate;
    }

    console.log('toAmount', toAmount);
    console.log('fromAmount', fromAmount);
    console.log('toamoutn');
    useEffect(() => {
        setLoading(true)
        console.log("here i n fetch data")
        axios
            .get(
                'http://data.fixer.io/api/latest?access_key=8fe97d1c43757594092a2b64639e006b',
            )
            .then(function (response) {
                console.log('respo', response);
                console.log('respo', Object.keys(response.data.rates)[0]);
                const firstCurrency = Object.keys(response.data.rates)[0];
                setCurrencyOptions([
                    response.data.base,
                    ...Object.keys(response.data.rates),
                ]);
                setFromCurrency(response.data.base);
                setToCurrency(firstCurrency);
                setExchangeRate(response.data.rates[firstCurrency]);
                setLoading(false)
            })
            .catch(function (error) {
                console.log(error);
            });
    }, []);

    useEffect(() => {
        console.log('fromCurrency', fromCurrency, 'toCurrency', toCurrency);
        if (fromCurrency != null && toCurrency != null) {
            axios
                .get(
                    `http://data.fixer.io/api/symbols?access_key=8fe97d1c43757594092a2b64639e006b&from=${fromCurrency}&to=${toCurrency}&amount=25`,
                )
                .then(response => {
                    console.log('exchange', response);
                })
                .catch(function (error) {
                    console.log(error);
                });
        }
    }, [fromCurrency, toCurrency]);
    function handleFromAmountChange(e) {
        setAmount(e);
        setAmountInFromCurrency(true);
    }

    function handleToAmountChange(e) {
        setAmount(e);
        setAmountInFromCurrency(false);
    }

    return (
        <View
            style={[styles.containerStyle, {backgroundColor: theme.backgroundColor}]}>
            <Loader loading={loading}/>
            <TouchableOpacity
                onPress={() => navigation.navigate('Option')}
                style={{
                    height: hp(7),
                    width: wp(15),
                    justifyContent: 'center',
                    alignItems: 'center',
                    alignSelf: 'flex-end',
                }}>
                <Image source={Images.setting} style={styles.settingButton}/>
            </TouchableOpacity>
            <Image
                source={Images.logo}
                style={styles.ImageStyle}
                resizeMode="contain"
            />
            <Text style={styles.textStyle}>Currency Converter</Text>
            <CurrencyRow currencyOptions={currencyOptions}
                         pickerColor={theme.pickerColor}
                         selectedCurrency={fromCurrency}
                         onChangeCurrency={base => setFromCurrency(base)}
                         onChangeAmount={handleFromAmountChange}
                         textColor={theme.color}
                         amount={fromAmount}/>

            <CurrencyRow
                pickerColor={theme.pickerColor}
                textColor={theme.color}
                currencyOptions={currencyOptions}
                selectedCurrency={toCurrency}
                onChangeCurrency={to => setToCurrency(to)}
                onChangeAmount={handleToAmountChange}
                amount={toAmount}/>


        </View>
    );
}

const styles = {
  containerStyle: {
    backgroundColor: '#506d79',
    height: hp(100),
    // alignItems: 'center',
  },
  ImageStyle: {
    height: hp(25),
    width: wp(40),
    alignSelf: 'center',

  },
  textStyle: {
    color: theme.color,
    fontSize: 32,
      marginTop: hp(5),
      marginBottom: hp(2),
    alignSelf: 'center',
  },
  inputContainerStyle: {
    width: wp(90),
    backgroundColor: 'white',
    flexDirection: 'row',
    borderRadius: 5,
    marginTop: 5,
    alignSelf: 'center',
  },
  currencyTextContainerStyle: {
    borderRightWidth: 1,
    justifyContent: 'center',
    alignItems: 'center',
    width: wp(25),
    borderColor: 'white',
    borderRightColor: 'black',
  },
  settingButton: {
    height: hp(100),
    width: wp(7),
    resizeMode: 'contain',
    tintColor: 'white',
  },
};
export default withTheme(Home);
