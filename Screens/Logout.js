import React, {Component} from 'react';

import AsyncStorage from '@react-native-community/async-storage';
import {View} from 'react-native';

class Logout extends Component {
  componentDidMount() {
    AsyncStorage.removeItem('USER').then(() => {
      this.props.navigation.navigate('Login');
    });
  }
  render() {
    return <View />;
  }
}

export default Logout;
