import {Image, Text, TouchableOpacity, View} from 'react-native';
import React, {Component} from 'react';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';

import Images from '../Images/image';

class Header extends Component {
  render() {
    return (
      <View
        style={{
          height: 64,
          marginBottom: '3%',
          borderBottomWidth: 1,
          flexDirection: 'row',
          borderColor: '#f3f3f3',
          backgroundColor: '#f3f3f3',
          elevation: 1,
        }}>
        <TouchableOpacity
          onPress={this.props.onPress}
          style={{
            width: '15%',
            height: '100%',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Image
            source={Images.arrowb}
            resizeMode="contain"
            style={{height: hp(10), width: wp(10), tintColor: '#0077cc'}}
          />
        </TouchableOpacity>
        <View
          style={{
            width: '70%',
            height: '100%',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text style={{fontSize: 32, fontWeight: 'bold'}}>
            {this.props.headerText}
          </Text>
        </View>
        <View
          style={{
            width: '15%',
            height: '100%',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          {/* <Image source={Images.setting} /> */}
        </View>
      </View>
    );
  }
}

export default Header;
