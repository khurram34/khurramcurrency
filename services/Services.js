import AsyncStorage from '@react-native-community/async-storage';
import firebase from 'react-native-firebase';

class Services {
  createNewUser = (email, password, callback) => {
    firebase
      .auth()
      .createUserWithEmailAndPassword(email, password)
      .then(userInfo => {
        callback({
          isSuccess: true,
          response: userInfo.user._user,
          uid: userInfo.user._user.uid,
        });
      })
      .catch(error => {
        callback({isSuccess: false, error: error.message});
      });
  };
  userProfile = (id, email, name) => {
    return firebase
      .firestore()
      .collection('Profile')
      .doc(id)
      .set({
        email: email,
        name: name,
        customer: false,
      })
      .catch(error => {
        console.log('ERROR-->>>>', error);
      });
  };
  userLogin = (email, password, callback) => {
    firebase
      .auth()
      .signInWithEmailAndPassword(email, password)
      .then(response => {
        callback({isSuccess: true, response: response.user._user});
      })
      .catch(error => {
        callback({isSuccess: false, error: error});
      });
  };
  getTheme = callback => {
    AsyncStorage.getItem('backgroundColor').then(res => {
      callback({isSuccess: true, theme: res});
    });
  };
  updateTheme = callback => {
    this.getTheme(res => {
      console.log('theme', res);
      if (res.isSuccess) {
        callback({backgroundColor: res.theme});
      }
    });
  };
}

Services = new Services();
export default Services;
