const Images = {
  logo: require('../Images/logo.png'),
  setting: require('../Images/setting.png'),
  reverse: require('../Images/reverse.png'),
  arrowf: require('../Images/arrowf.png'),
  arrowb: require('../Images/arrowb.png'),
  logout: require('../Images/logout.png'),
  check: require('../Images/check.png'),
  uncheck: require('../Images/uncheck.png'),
};

export default Images;
